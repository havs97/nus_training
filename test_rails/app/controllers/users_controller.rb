class UsersController < ApplicationController
	def index
		flash[:notice] = t(:hello_flash)
	end

	def new
		@user = User.new
	end
end