$().ready(function() {
	$("#signupForm").validate({
		rules: {
			firstname: {
				required: true,
				maxlength: 25
			},
			lastname: {
				required: true,
				maxlength: 25
			},
			password: {
				required: true,
				maxlength: 64
			},
			password_confirm: {
				required: true,
				maxlength: 64,
				equalTo: "#password"
			}
		},
		messages: {
			firstname: {
				required: "Please enter a First Name",
				maxlength: "Your First Name maximum 25 characters long"
			},
			lastname: {
				required: "Please enter a Last Name",
				maxlength: "Your Last Name maximum 25 characters long"
			},
			password: {
				required: "Please provide a password",
				maxlength: "Your Password maximum 64 characters long"
			},
			password_confirm: {
				required: "Please provide a password",
				maxlength: "Your Password maximum 64 characters long",
				equalTo: "Please enter the same password as above"
			}
		}
	});
});