
class EmailValidator < ActiveModel::EachValidator
	def validate_each(record, attribute, value)
		unless value =~ /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
			record.errors[attribute] << (options[:message] || "is not an email")
		end
	end
end

class User < ApplicationRecord
	validates :email, uniqueness: true, email: true, presence: true
	#validates_with GoodnessValidator
	#validates :email, inclusion: { in: %w{	gmail com}}
	#validates :phone, numericality: true

	#callback
	after_initialize do |user|
    	puts "You have initialized an object!"
  	end

	after_find do |user|
	    puts "You have found an object!"
	end

  	after_touch do |user|
    	puts "You have touched an object!"
  	end

end
