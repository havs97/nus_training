class Album < ApplicationRecord
	enum sharing_mode: {public: 1, private: 0}
	has_many :images, as: :imageable
end
