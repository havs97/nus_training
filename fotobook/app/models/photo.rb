class Photo < ApplicationRecord
	enum sharing_mode: {public: 1, private: 0}
	has_one :image, as: :imageable
end
